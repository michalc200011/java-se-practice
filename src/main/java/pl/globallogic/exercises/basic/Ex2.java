package pl.globallogic.exercises.basic;

import static java.lang.Math.round;

public class Ex2 {

    public static long toMilesPerHour(double kilometersPerHour){
        if(kilometersPerHour >=0) return round(kilometersPerHour/1.609344);
        return -1;
    }
    public static void printConversion(double kilometersPerHour){
        if(kilometersPerHour >=0) {
            System.out.println(kilometersPerHour +" km/h = "+toMilesPerHour(kilometersPerHour)+" mi/h");
            return;
        }
        System.out.println("Error, parameter < 0");
    }
    public static void main(String[] args) {
        System.out.println(toMilesPerHour(1.5));
        System.out.println(toMilesPerHour(10.25));
        System.out.println(toMilesPerHour(-5.6));
        System.out.println(toMilesPerHour(25.42));
        System.out.println(toMilesPerHour(75.114));
        printConversion(1.5);
        printConversion(10.25);
        printConversion(-5.6);



    }
}
