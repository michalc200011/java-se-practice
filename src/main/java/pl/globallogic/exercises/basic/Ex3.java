package pl.globallogic.exercises.basic;

public class Ex3 {
    public static void printMegaBytesAndKiloBytes(int kiloBytes){
        if(kiloBytes<0){
            System.out.println("Invalid Value");
            return;
        }
        System.out.println(kiloBytes/1024+" MB and "+kiloBytes%1024+" MB");
    }
    public static void main(String[] args) {
        printMegaBytesAndKiloBytes(2500);
    }
}
