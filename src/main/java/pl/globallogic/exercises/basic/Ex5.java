package pl.globallogic.exercises;

public class Ex5 {
    public static boolean isLeapYear(int year){
        if(year >0 && year <=9999){
            return year%4==0 && year%100==0 && year%400==0;
        }else return false;
    }
    public static void main(String[] args) {
        System.out.println(isLeapYear(1700));
        System.out.println(isLeapYear(1600));
        System.out.println(isLeapYear(0));
        System.out.println(isLeapYear(10000));
    }
}
