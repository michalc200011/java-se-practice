package pl.globallogic.exercises;

public class Ex9 {
    public static double area(double radius){
        return radius>-1? radius*radius*Math.PI : -1;
    }
    public static double area(double x,double y){
        return x>-1&&y>-1? x*y : -1;
    }
    public static void main(String[] args) {
        System.out.println(area(5.0));
        System.out.println(area(-1));
        System.out.println(area(5.0, 4.0));
        System.out.println(area(-1.0, 4.0));

    }
}
