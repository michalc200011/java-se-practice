package pl.globallogic.exercises;

public class Ex10 {
    public static void printYearsAndDays (long minutes){
        long days=minutes/(60*24);
        System.out.println(minutes>0?minutes+" min = "+days/365+" y and "+days%365+" d":"Invalid Value");
    }
    public static void main(String[] args) {
        printYearsAndDays(525600);
        printYearsAndDays(1051200);
        printYearsAndDays(561600);
    }
}
