package pl.globallogic.exercises.basic;

public class Ex1 {
    public static void checkNumbers (int number){
        /*
        "positive" if the parameter number is > 0
        "negative" if the parameter number is < 0
        "zero" if the parameter number is equal to 0
        */
        if(number>0)System.out.println("positive");
        if(number<0)System.out.println("negative");
        if(number==0)System.out.println("zero");
    }
    public static void main(String[] args) {
        checkNumbers(2);
        checkNumbers(0);
        checkNumbers(-1);
    }
}
