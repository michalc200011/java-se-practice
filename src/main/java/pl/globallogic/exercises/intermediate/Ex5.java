package pl.globallogic.exercises.intermediate;

import static java.lang.Math.pow;

public class Ex5 {
    public static int  sumFirstAndLastDigit(int number){
        if(number<0)return -1;
        int size=Integer.toString(number).length();
        return (int)(number/pow(10,size-1) +number%10);
    }
    public static void main(String[] args) {
        System.out.println(sumFirstAndLastDigit(101));
        System.out.println(sumFirstAndLastDigit(257));
        System.out.println(sumFirstAndLastDigit(0));
        System.out.println(sumFirstAndLastDigit(-10));
    }
}
