package pl.globallogic.exercises.intermediate;

import com.sun.jdi.IntegerValue;

public class Ex6 {
    public static int getEvenDigitSum(int number){
        if(number<0)return -1;
        int sum=0,len=Integer.toString(number).length()-1;
        while(len!=0){
            if(Integer.parseInt(String.valueOf(Integer.toString(number).charAt(len)))%2==0){
                sum += Integer.parseInt(String.valueOf(Integer.toString(number).charAt(len)));
            }
            len--;
        }return sum;

    }
    public static void main(String[] args) {
        System.out.println(getEvenDigitSum(123456789));
        System.out.println(getEvenDigitSum(252));
        System.out.println(getEvenDigitSum(-22));
    }
}
