package pl.globallogic.exercises.intermediate;

public class Ex4 {
    public static boolean isPalindrome(int number){
        String txt=Integer.toString(number);
        int i = number<0? 1:0,j=txt.length()-1;
        while(i<j){
            if(txt.charAt(i)!=txt.charAt(j))return false;
            i++;
            j--;
        }return true;
    }
    public static void main(String[] args) {
        System.out.println(isPalindrome(-1221)==true);
        System.out.println(isPalindrome(707)==true);
        System.out.println(isPalindrome(11212)==false);
    }
}
