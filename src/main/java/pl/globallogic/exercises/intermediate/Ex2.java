package pl.globallogic.exercises.intermediate;

public class Ex2 {
    public static boolean isLeapYear(int year){
        if(year >0 && year <=9999){
            return year%4==0 && year%100==0 && year%400==0;
        }else return false;
    }
    public static int getDaysInMonth (int month,int year){
        if(month<1||month>12||year<1||year>9999)return -1;
        if(isLeapYear(year)&&month==2)return 29;
        return month==2 ? 28:30+month%2;
    }
    public static void main(String[] args) {
        System.out.println(getDaysInMonth(1,1000)==31);
        System.out.println(getDaysInMonth(2,1600)==29);
        System.out.println(getDaysInMonth(2,1000)==28);
        System.out.println(getDaysInMonth(4,1000)==30);
        System.out.println(getDaysInMonth(-1, 2020)==-1);
    }
}
